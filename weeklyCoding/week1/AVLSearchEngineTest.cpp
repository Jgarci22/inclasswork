/**********************************************
 File: AVLSearchEngineTest.cpp
* Author: Jorge Garcia
* Email: Jgarci22@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct URL {
				std::string url;
				std::string webName;

				URL(std::string url, std::string webName) : url(url), webName(webName) {}

				/********************************************
				* Function Name  : operator<
				* Pre-conditions : const URL& rhs
				* Post-conditions: bool
				* Struct comparison
				********************************************/
				bool operator<(const URL& rhs) const{
								if (webName < rhs.webName)
												return true;

								else if (webName == rhs.webName) {
												if (url < rhs.url) 
																return true;
								}

								return false;
				}

				/********************************************
				* Function Name  : operator==
				* Pre-conditions : const URL& rhs
				* Post-conditions: bool
				* Struct comparison
				********************************************/
				bool operator==(const URL& rhs) const{
								if (webName != rhs.webName)
												return false;

								else {
												if (url != rhs.url)
																return false;
								}

								return true;
				}

				friend std::ostream& operator<<(std::ostream& outStream, const URL& printURL);
};

/********************************************
* Function Name  : operator<<
* Pre-conditions : std::ostream& outStream, const URL& printURL
* Post-conditions: std::ostream&
* Operator overload to structure output
********************************************/
std::ostream& operator<<(std::ostream& outStream, const URL& printURL) {
				outStream << printURL.url << ", " << printURL.webName;

				return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{	
		AVLTree<URL> avlURL;

    URL url1("https://cse.nd.edu", "Notre Dame CSE Homepage");
		URL url2("https://www.nd.edu", "Notre Dame Homepage");
		URL url3("https://www.youtube.com", "Youtube Homepage");
		URL url4("https://www.google.com", "Google Homepage");
		URL url5("https://www.espn.com", "ESPN Homepage");
		URL url6("https://www.yahoo.com", "Yahoo! Homepage");
		URL url7("https://sakai.nd.edu", "Sakai Homepage");
		URL url8("https://inside.nd.edu", "InsideND Homepage");
		URL url9("https://www.reddit.com", "Reddit Homepage");
		URL url10("https://twitter.com", "Twitter Homepage");
		URL url11("https://twitter.com", "Twitter Homepage"); // Duplicate

		avlURL.insert(url1);
		avlURL.insert(url2);
		avlURL.insert(url3);
		avlURL.insert(url4);
		avlURL.insert(url5);
		avlURL.insert(url6);
		avlURL.insert(url7);
		avlURL.insert(url8);
		avlURL.insert(url9);
		avlURL.insert(url10);
		avlURL.insert(url11);

		avlURL.printTree();

		avlURL.remove(url2);

		std::cout << "-----" << std::endl;

		avlURL.printTree();
	
    return 0;
}
