/**********************************************
* File: wordFrequency.cpp
* Author: Jorge Garcia
* Email: jgarci22@nd.edu
*  
* Method to find the frequency of occurrences of any given word in a book
**********************************************/

#include <iostream>
#include <fstream>
#include <map>
#include <unordered_map>
#include <string>
#include <string>

int main(int argc, char** argv)
{
				// Initialize hash map with string index and word count key
				std::unordered_map<std::string, int> wordCount;

				// Open file to be read and scanned
				std::ifstream file;
				file.open(argv[1]);
				std::string word;
				std::string given = argv[2];

				// Read opened file
				std::cout << "File text: ";
				while (file >> word) {	
								std::cout << word << " ";		// Outputs file text to terminal
								if (wordCount.count(word) == 0)		// If current word being read from file has not been counted yet, count now
												wordCount[word] = 1;
								else 
												wordCount[word] = wordCount[word] + 1;
				}

				std::cout << std::endl << std::endl;
				file.close();
				std::cout << given << " count is: " << wordCount[given] << std::endl;

				return 0;
}
