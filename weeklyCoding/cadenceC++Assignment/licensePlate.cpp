// licensePlate.cpp

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <iterator>
#include <cctype>
#include <fstream>

bool sorting(std::string one, std::string two) {
				if (one.length() == two.length())
								return one < two;

				return one.length() < two.length();
}

int main(int argc, char **argv) {
				// check if user has inputted a license plate as an argument
				if (argc < 2) {
								std::cout << "Insert a license plate: " << std::endl;
								return 0;
				}

				// declare variables and structures
				std::vector<std::string> dict;
				std::ifstream file;
				file.open("words.txt");
				std::string plate = argv[1];
				std::string lplate;
				std::string word;
				std::string target;
				int status = false;

				// place plate characters into vector
				for (auto it = plate.begin(); it < plate.end(); it++) {
								if (isalpha(*it))
												lplate += *it;
				}

				while (file >> word) {
								if (word.length() >= lplate.length())
												dict.push_back(word);
				}
				
				std::sort (dict.begin(), dict.end(), sorting);

				// iterate words
				for (auto it = dict.begin(); it != dict.end(); it++) {
								for (auto ct = lplate.begin(); ct < lplate.end(); ct++) {
												// count algorithm to check if dict word contains as many or more chars that appear in license plate
												if (std::count( (*it).begin(), (*it).end(), (*ct) ) >= std::count (lplate.begin(), lplate.end(), (*ct) ) )
																status = true;
												else {
																status = false;
																break;
												}
								}

								if (status == true) {
												target = *it;
												break;
								}
				}

				std::cout << target << std::endl;

				return 0;
}
