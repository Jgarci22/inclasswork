/**********************************************
* File: remCycle.cpp
* Author: Jorge Garcia
* Email: jgarci22@nd.edu
* Given a directed graph, program detects and removes any cycle
*
* Citation: some code borrowed from https://www.geeksforgeeks.org/detect-cycle-in-a-graph/; however, I made sure to fully understand the code before using implementing it into the program and it greatly helped me better learn the concepts discussed in class.
**********************************************/

#include <iostream>
#include <list>
#include <limits.h>

using namespace std;

int init, fin;

class Graph 
{ 
				int V;     
				list<int> *adj;   
				bool isCyclicUtil(int v, bool visited[], bool *rs);   
				public: 
				Graph(int V); 
				void addEdge(int v, int w);    
				void removeEdge(int v, int w); 
				bool isCyclic();    
};

/********************************************
* Function Name  : Graph::Graph
* Pre-conditions : int V
* Post-conditions: none
* constructor  
********************************************/
Graph::Graph(int V) 
{ 
				this->V = V; 
				adj = new list<int>[V]; 					
} 

/********************************************
* Function Name  : Graph::addEdge
* Pre-conditions : int v, int w
* Post-conditions: none
* adds edge to graph 
********************************************/
void Graph::addEdge(int v, int w) 
{ 
				adj[v].push_back(w); 									
} 

/********************************************
* Function Name  : Graph::removeEdge
* Pre-conditions : int v, int w
* Post-conditions: none
* deletes edge from graph 
********************************************/
void Graph::removeEdge(int v, int w){
				list<int>::iterator it = adj[v].begin();
				for(int i = 0; i < adj[v].size(); i++){
								if( *it == w ){
												adj[v].erase(it);
												break;
								}
								advance(it, 1);
				}
}

/********************************************
* Function Name  : Graph::isCyclicUtil
* Pre-conditions : int v, bool visited[], bool *recStack
* Post-conditions: bool
* makes a stack of nodes to determine if a cycle exists in a graph  
********************************************/
bool Graph::isCyclicUtil(int v, bool visited[], bool *recStack) { 
				if(visited[v] == false) {    // mark the current node as visited and part of recursion stack
								visited[v] = true; 
		     	 			recStack[v] = true;  
								list<int>::iterator i; 
								for(i = adj[v].begin(); i != adj[v].end(); ++i) { 
												if ( !visited[*i] && isCyclicUtil(*i, visited, recStack)){ 
																return true; 
												}
												
												else if (recStack[*i]){    
																init = recStack[*i];
																fin = v;
																cout << "\nVertices " << init << " and " << fin << " make a cycle." << endl;
																return true;		   	
												}                                                                        
								}
				} 
				recStack[v] = false;   
				return false; 
} 

/********************************************
* Function Name  : Graph::isCyclic
* Pre-conditions : none
* Post-conditions: bool
* returns true/false value depending on whether graph is cyclic 
********************************************/
bool Graph::isCyclic() {
				bool *visited = new bool[V]; 
       	bool *recStack = new bool[V]; 
	      for(int i = 0; i < V; i++) { 
								visited[i] = false; 
								recStack[i] = false; 
				} 
	                                                                                                                  
	 // call the recursive helper function to detect cycle in different DFS trees 
        for(int i = 0; i < V; i++) 
								if (isCyclicUtil(i, visited, recStack)) 
												return true; 
				return false; 
}


/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* main driver function that checks directed graph and removes cycle, if present 
********************************************/
int main() { 
				// create cyclic graph and test
			  Graph g1(5); 
				g1.addEdge(1, 0); 
				g1.addEdge(0, 2);  
				g1.addEdge(2, 0); 
				g1.addEdge(0, 3); 
		  	g1.addEdge(3, 4); 

				if(g1.isCyclic()){
								g1.removeEdge(init, fin);
								cout << "Edges that created a cycle, " << init << " and " << fin << ", removed." << endl;
				}
				else {
								cout << "Graph does not contain a cycle." << endl;
				}

				// create non-cyclic graph and test
				Graph g2(3);
				cout << "\n-----------------------------------------------------------\n";
				g2.addEdge(0, 1);
				g2.addEdge(1, 2);

				if(g2.isCyclic()) {
								g2.removeEdge(init, fin);
								cout << "Edges that created a cycle, " << init << " and " << fin << ", removed." << endl;
				}
				else {
								cout << "\nGraph does not contain a cycle." << endl;
				}
				return 0;                                                           
} 
