/**********************************************
* File: AVLStrTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<std::string> testAVL;
    
	std::cout << "Testing testAVL" << std::endl;
	testAVL.insert( "Wake" );
	testAVL.insert( "up" );
	testAVL.insert( "the" );
	testAVL.insert( "the" );  // Test for no duplicates
	testAVL.insert( "echoes" );
	testAVL.insert( "cheering" );
	testAVL.insert( "her" );
	testAVL.insert( "name" );
	testAVL.printTree( );
	
	std::cout << "The maximum value is " << testAVL.findMax() << std::endl;
	std::cout << "The minimum value is " << testAVL.findMin() << std::endl;
	
	std::cout << "The test for whether testAVL contains echoes returns " << testAVL.contains( "echoes" ) << std::endl;
	std::cout << "The test for whether testAVL contains Notre returns " << testAVL.contains( "Notre" ) << std::endl;
	
	std::cout << "Testing testAVL2 after removing Wake" << std::endl;
	AVLTree<std::string> testAVL2(testAVL);
	testAVL2.remove("Wake");
	testAVL2.printTree( );
	std::cout << "The test for whether testAVL2 contains Wake returns " << testAVL2.contains( "Wake" ) << std::endl;
	
    return 0;
}
